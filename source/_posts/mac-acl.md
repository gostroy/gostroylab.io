---
title: MAC.ACL
date: 2019-02-09
---

recursive access to specific directory for user on MacOS
```
chmod -R +a "alexb allow write,add_file,add_subdirectory" ~/code/study-search-service
```
