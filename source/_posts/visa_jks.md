---
title: VISA Direct + JKS
date: 2019-11-01
---

## create JKS
during create project in sandbox save your private key - **privateKey.pem**
https://developer.visa.com/ -> Credentials -> download
 * Project Certificate - **cert.pem**
 * Common Certificates - **VDPCA-SBX.pem, DigiCertGlobalRootCA.crt**

```
openssl pkcs12 -export -in cert.pem -inkey "privateKey.pem" -certfile cert.pem -out key.p12
keytool -importkeystore -srckeystore key.p12 -srcstoretype PKCS12 -destkeystore key.jks
keytool -import -alias ejbca -keystore key.jks -file VDPCA-SBX.pem -storepass <KEYSTORE_PASSWORD>
keytool -import -alias digica -keystore key.jks -file DigiCertGlobalRootCA.crt -storepass <KEYSTORE_PASSWORD>
```