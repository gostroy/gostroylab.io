---
title: Speaker Diarization
date: 2017-05-16
---
 разделения входящего аудиопотока на однородные сегменты в соответствии с принадлежностью аудиопотока тому или иному говорящему. В Java можно использовать [LIUM_SpkDiarization](http://www-lium.univ-lemans.fr/diarization/doku.php/welcome)

и небольщая свобка комманд
```
http://audio.online-convert.com/ru/convert-to-wav  - конвертнуить в МОНО(для Goofle Cloud Speech) WAV (для LIUM_SpkDiarization)

> java -Xmx2024m -jar c:\java\LIUM_SpkDiarization-8.4.1.jar --fInputMask=./clothes.wav --doCEClustering --sOutputFormat seg.xml clothes   # result timestamp в виде start-end
> java -Xmx2024m -jar c:\java\LIUM_SpkDiarization-8.4.1.jar --fInputMask=./clothes.wav --doCEClustering clothes   # result timestamp в виде start-duration
> cat clothes.out.seg

#ffmpeg -i clothes.flac -f segment -segment_time 30 -c copy out%03d.flac  # разбить на части по 30сек
ffmpeg  -i clothes.wav -ss [start] -t [duration] -c copy clothes000.wav # разбить на части от start длинной duration

ffmpeg  -i clothes.wav -ss 3.18 -t 4.85 -c copy clothes000.wav
ffmpeg  -i clothes.wav -ss 8.44 -t 4.35 -c copy clothes001.wav
...
```