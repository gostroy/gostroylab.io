---
title: java
date: 2018-01-14
---

## jenv

для установки в MacOS нескольких версий java паралельно 
```
$ brew cask install java
$ brew install jenv
$ echo  >> ~/.zshrc
$ echo 'export PATH="$HOME/.jenv/bin:$PATH"' >> ~/.zshrc
$ echo 'eval "$(jenv init -)"' >> ~/.zshrc

$ ls /Library/Java/JavaVirtualMachines/
$ jenv add /Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home
$ jenv add /Library/Java/JavaVirtualMachines/jdk1.7.0_80.jdk/Contents/Home
$ jenv add /Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home
$ jenv rehash

$ jenv versions

$ java -version
$ jenv global oracle64-1.8.0.131

$ cd $ODIN_HOME
$ jenv local oracle64-1.7.0.80
$ cat .java-version
oracle64-1.7.0.80

$ java -version
java version "1.7.0_80"
Java(TM) SE Runtime Environment (build 1.7.0_80-b15)
Java HotSpot(TM) 64-Bit Server VM (build 24.80-b11, mixed mode)
$ cd ..
$ java -version
java version "1.8.0_131"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)

# for startup scripts used by Tomcat, sbt and others
mPolitech:odin-home panser$ echo $JAVA_HOME

mPolitech:odin-home panser$ jenv exec bash
bash-3.2$ echo $JAVA_HOME
/Users/panser/.jenv/versions/oracle64-1.7.0.80
bash-3.2$
```

## sdkman
```
sdk list java
sdk install java 17-open 
sdk default java 17-open
sdk use java 17-open
```
...
```
sdk install java my-local-13 /Library/Java/JavaVirtualMachines/jdk-13.jdk/Contents/Home
sdk use java my-local-13
```

## brew
```
brew update
brew tap adoptopenjdk/openjdk

brew search jdk
brew info adoptopenjdk

brew cask install adoptopenjdk13
```