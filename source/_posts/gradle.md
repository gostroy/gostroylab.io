---
title: Gradle
date: 2017-03-28
---

мощный build manager

создаем проект
```
> gradle wrapper --gradle-version 3.4.1
> gradlew init --type java-application

> gradlew -Pversion=1.3.4 build
```

интересные комманды/опции
```
> gradlew tasks
:tasks
...

> gradlew dependencies --configuration compile
...
> gradlew --gui
...
> gradlew --offline
...
> groovysh  # интерактивный shell для groovy, в нем можно тестировать таски из gradle

> gradlew assemble --profile # создает html отчет со временем сборки всех шагов

> gradlew --refresh-dependencies clean  # обновить все зависимости. Удобно использовать при повреждении данных, находящихся в кэше. Верифицирует кэшированные данные и при отличии обновляет их.
```


[Способы подключения различных зависимостей: внешние зависимости, проектные зависимости, файловые зависимости. Зависимости(модули) можно подменять на свои.](https://habrahabr.ru/company/redmadrobot/blog/275515/)

