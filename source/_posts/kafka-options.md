---
title: Kafka. Options
date: 2019-02-03
---

### Spring Cloud Stream
kafka
  * **session.timeout.ms = 10000** интервал ответа на heartbeat. до версии <=0.10.0 он так же отвечал за максимальное время обработки сообщения, позже появился **max.poll.interval.ms**.  необходимо установить session.timeout.ms достаточно большим, чтобы сбои при перебалансировках происходили реже. Как упомянуто выше, единственным недостатком этого является более длительная задержка переназначения партиций в случае серьезного сбоя (когда потребитель не может быть чисто завершен с помощью close()), что на практике редко происходит.
  * **max.poll.interval.ms = 300000** >=0.10.1 время обработки сообещения консюмером
  * **max.poll.records = 500** количество записей на одну обработку
  * **heartbeat.interval.ms = 3000** контролирует, как часто потребитель должен отправлять heartbeats-сообщения координатору. Это также способ, когда необходимость перебалансировки определяется засчет потребителя, поэтому более короткий интервал heartbeats-сообщений обычно означает более быструю перебалансировку.
  * **enable.auto.commit** потребитель автоматически фиксирует смещения с заданным в **auto.commit.interval.ms** интервалом (по умолчанию – 5 секунд) 
  * **auto.offset.reset** определяет поведение потребителя, когда нет зафиксированной позиции смещения (в случае, когда группа инициализируется впервые), или когда оно выходит за пределы диапазона. Можно установить сброс положения на самое раннее смещение – **earliest**, либо на самое позднее – **latest** (задано по умолчанию). Также можно выбрать значение **none** для самостоятельной установки начального смещения и ручной обработки ошибок вне диапазона.
  * **max.partition.fetch.bytes** чтобы ограничить объем данных, возвращаемых в одном пакете, но при этом приходится учитывать, сколько партиций содержится в подписанных топиках.
  * **fetch.min.bytes**  объема данных, возвращаемых в каждом poll()
  * **fetch.max.wait.ms** 
  
producer
  * **request.required.acks =**
    * **0** не ждать подтверждения о записи сообщения в брокер
    * **1** ждать подтверждения после записи в master брокер
    * **-1** ждать подтверждения после записи во все синхронные партиции (**min.insync.replicas.per.topic**)
    * **all** – сообщение не только допущено к записи лидером партиции, но и успешно скопировано на все синхронизированные реплики

#### Spring Cloud Stream
binder
  * **spring.cloud.stream.kafka.binder.configuration:**  - key/value map of client properties (both producers and consumer)
  * **spring.cloud.stream.kafka.binder.autoCreateTopics: true**  the binder will create new topics automatically. Of note, this setting is independent of the **auto.topic.create.enable** setting of the broker and it does not influence it: if the server is set to auto-create topics, they may be created as part of the metadata retrieval request, with default broker settings.
  * **spring.cloud.stream.kafka.binder.autoAddPartitions: false**  

consumer
  * **spring.cloud.stream.kafka.bindings.<channelName>.consumer.autoCommitOffset: true** - делать ли авто-ACK сообщения после его обработки, или посылать этот ACK вручную из кода (AckMode.MANUAL)
  * **spring.cloud.stream.kafka.bindings.<channelName>.consumer.enableDlq: false** - создавать ли отдельную очередь и помещать ли туда сообщения которые неполучилось обработать
  * **spring.cloud.stream.kafka.bindings.<channelName>.consumer.configuration:** - key/value pair containing generic Kafka consumer properties    

producer
  * **spring.cloud.stream.kafka.bindings.<channelName>.producer.configuration:**  - key/value pair containing generic Kafka producer properties