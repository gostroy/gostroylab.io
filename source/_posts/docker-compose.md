---
title: Docker-Compose
date: 2017-04-08
---

```
# создания ф-ла конфигурации с запускаемыми image
mkdir repEngine
cd repEngine
cp ~/repEngine.war .
vim docker-compose.yml
 version: '2'
 services:
   repengine-java:
     image: panser/repengine-java
     ports:
      - &amp;amp;amp;quot;8080:8080&amp;amp;amp;quot;
     volumes:
      - .:/docker
     depends_on:
      - repengine-mysql
   repengine-mysql:
     image: panser/repengine-mysql
 
# запустить
docker-compose up
# запустить в фоне
docker-compose up -d
# запустить ранее созданые контейнеры, не создавая их из image
docker-compose start
# потушить
docker-compose stop
 
# просмотр запущенных контейнеров
docker-compose ps
 
# просмотр переменных запуска для контейнера
docker-compose run repengine-mysqlenv
 
# запуск с несколькими ф-лами конфигурации под разные env
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

```
