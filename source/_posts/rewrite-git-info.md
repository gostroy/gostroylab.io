---
title: Change Git Log Info
date: 2016-11-02
---

```
$ git filter-branch -f --env-filter "`cat fix-git.txt`" -- --all
```
<!--more-->

```
cat fix-git.txt

WRONG_EMAIL="wrong@example.com"
WRONG_NAME="Wrong Name"
NEW_NAME="Correct Name"
NEW_EMAIL="correct@example.com"

if [ "$GIT_COMMITTER_EMAIL" == "$WRONG_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$NEW_NAME"
    export GIT_COMMITTER_EMAIL="$NEW_EMAIL"
fi

if [ "$GIT_AUTHOR_EMAIL" == "$WRONG_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$NEW_NAME"
    export GIT_AUTHOR_EMAIL="$NEW_EMAIL"
fi


if [ "$GIT_COMMITTER_NAME" == "$WRONG_NAME" ]
then
    export GIT_COMMITTER_NAME="$NEW_NAME"
    export GIT_COMMITTER_EMAIL="$NEW_EMAIL"
fi

if [ "$GIT_AUTHOR_NAME" == "$WRONG_NAME" ]
then
    export GIT_AUTHOR_NAME="$NEW_NAME"
    export GIT_AUTHOR_EMAIL="$NEW_EMAIL"
fi
```

[from here](https://www.git-tower.com/learn/git/faq/change-author-name-email)