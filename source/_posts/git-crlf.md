---
title: GIT. CRLF
date: 2017-11-05
---

```
$ rm .git/index
$ git reset
```

[https://help.github.com/articles/dealing-with-line-endings/] (https://help.github.com/articles/dealing-with-line-endings/)

