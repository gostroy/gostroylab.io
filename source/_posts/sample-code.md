---
title: Sample Code
date: 2016-10-25
---
Here I collect links on small projects which I did during learning technologies
<!--more-->

**Back-end**
  * small app wich export rest resources (**Spring MVC**), based on **Spring-Boot** and for documantation rest use **Swagger** with custom exception handling, all data save in **MongoDB** and for data access used **Spring-Data**. [src](https://gitlab.com/panser-portfolio/rest-air-app)  
  * one more application based on **Spring-Boot** to upload pack of photos and show them in web page. [src](https://github.com/panser/mimicFlickr)

**JS Front-end / Mobile:**
  * Angular
    * CRUD with model windows on angular1, for server side use JS json-server. [src](https://gitlab.com/panser-portfolio/projectstatus/tree/master)
    * **https://panser-portfolio.gitlab.io/devranking/**  app to try REST (GET) with angular, routing and different data bindings [source](https://gitlab.com/panser-portfolio/devranking/tree/master)
  * Angular2
    * **http://panser-portfolio.gitlab.io/angular2tryapps/app1_simpleWebsite/**  project to try Angular2 application [source](https://gitlab.com/panser-portfolio/angular2tryapps/tree/master/app1_simpleWebsite)
    * **http://panser-portfolio.gitlab.io/angular2tryapps/app2-githubsearch/**  developer profile search using the Github API [source](https://gitlab.com/panser-portfolio/angular2tryapps/tree/master/app2-githubsearch)
    * **http://panser-portfolio.gitlab.io/angular2tryapps/app3-ngspotify/**   album/artist application using the Spotify API [source](https://gitlab.com/panser-portfolio/angular2tryapps/tree/master/app3-ngspotify)
    * **http://panser-portfolio.gitlab.io/angular2tryapps/app5-firebasebuscont/**  app using Firebase [source](https://gitlab.com/panser-portfolio/angular2tryapps/tree/master/app5-firebasebuscont)
    * todos app using MongoDB, Express, Angular & Node.js [source](https://gitlab.com/panser-portfolio/angular2tryapps/tree/master/app4-meantodos)
  * Ionic
    * **http://panser-portfolio.gitlab.io/ionic-weather/**   weather App and 7-day forecast [source](https://gitlab.com/panser-portfolio/ionic-weather/tree/master)
    * **https://panser-portfolio.gitlab.io/pickmeup/** uber-clone app [source](https://gitlab.com/panser-portfolio/pickmeup/tree/master)  
  * Leaflet
    * map build with Leaflet and use two WMS map layers, TileLayer with some data from internal map server,  topoJSON on front to show borders and during move over the map it save all it current state in url. Backend wrote with spring-boot and it run PhantomeJs script to make screenshot for current map view with different resolution. [source](https://gitlab.com/panser-portfolio/pickmeup/tree/master) 
    
**CSS**
  * **https://panser-portfolio.gitlab.io/numedeon-demo/**  
  * **https://panser-portfolio.gitlab.io/numedeon-demo/demo/**  layout with image background from PSD to reduce time and save customer many. This page used just for demo on presentation.
  * **https://gostroy.gitlab.io/landing-fairlign/** landing page

**Static sites:**
  * Hexo
    * **https://gostroy.gitlab.io**  this blog, use [hypercomments](https://www.hypercomments.com/) for private comments. [source](https://gitlab.com/gostroy/gostroy.gitlab.io/tree/master)
    * **https://ivsite-hexo.gitlab.io/**  site based on hexo with menu-based content with using [Auth0](https://auth0.com/) for frontenf authorization and [disqus](https://disqus.com/) for leave comments. [source](https://gitlab.com/ivsite-hexo/ivsite-hexo.gitlab.io/tree/master)
  * Jekyll
    * **http://ivsite-jekyll.gitlab.io**  static site based on jekyll with few different collections using content from [DatoCms](https://www.datocms.com/), [hypercomments](https://www.hypercomments.com/) for private comments and local search with [lunr.js](http://lunrjs.com/). [source](https://gitlab.com/ivsite-jekyll/ivsite-jekyll.gitlab.io/tree/master)

