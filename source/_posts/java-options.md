---
title: Java Options
date: 2017-04-28
---

переменная среды, которая воспринимается всеми JVM машинами - **JAVA_TOOL_OPTIONS**

для настройки дебага
```
JAVA_TOOL_OPTIONS="-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=*:5005,suspend=n"
```

**и в дополнение**, популярная опция для >=Java6 `-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005`
 у меня не работала в Docker окружении. Поэтому использую старый вариант.


## MEM/CPU
проверить как java видит лимиты MEM/CPU в контейнере
```
java -XshowSettings:system -version
```
если в контенерах много ресурсов, то можно запусть с такой опцией
```
-XX:axRAMPercentage
```

