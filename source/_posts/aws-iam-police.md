---
title: AWS IAM Polices
date: 2016-10-26
---
config for add accees by ip adresses and aws login
<!--more-->
 ```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "FirstStatement",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "$AWS_LOGIN"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "arn:aws:es:eu-central-1:423995045840:domain/repengine-dev/*"
    },
    {
      "Sid": "SecondStatement",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:eu-central-1:423995045840:domain/repengine-dev/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": [
            "$IP1",
            "$IP2",
            "$IP3"
          ]
        }
      }
    },
    {
      "Sid": "StatementThree",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "es:*",
      "Resource": "arn:aws:es:eu-central-1:423995045840:domain/repengine-dev/*",
      "Condition": {
        "StringLike": {
          "aws:SourceVpc": "vpc-bbfdf8d2"
        }
      }
    }
  ]
}
```

[AWS Policy Generator](http://awspolicygen.s3.amazonaws.com/policygen.html)

[IAM Policy Simulator, check IAM Police](https://policysim.aws.amazon.com/home/index.jsp)

[IAM Policy Elements Reference](http://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements.html)

[Policies for VPC](http://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies-vpc-endpoint.html)