---
title: Amazon EC2 Container Service
date: 2017-04-07
---

сервис управления контейнерами, для разворачивания Docker кластера на AWS инфраструктуре (подобие 
Kubernetes, Mesos/Marathon, Docker Swarm)
<!--more-->

```
# Create a Cluster
> aws ecs create-cluster --cluster-name RepEngineCluster
# List Container Instances
> aws ecs list-container-instances --cluster RepEngineCluster

# install EC2 instance from `ecs-optimized AMI` http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI_launch_latest.html
# attach `ecsInstanceRole` to your new EC2 instance http://docs.aws.amazon.com/AmazonECS/latest/developerguide/instance_IAM_role.html

> ssh ecs-new-instance
$ sudo echo "ECS_CLUSTER=RepEngineCluster" >> /etc/ecs/ecs.config
$ sudo shutdown -r now

# Describe your Container Instance
> aws ecs describe-container-instances --cluster default --container-instances container_instance_ID

# Register a Task Definition
> aws ecs register-task-definition --cli-input-json file://Dockerrun.aws.json
# List Task Definitions
> aws ecs list-task-definitions

# Run a Task
> aws ecs run-task --task-definition sleep360:1 --count 1

# List Tasks
> aws ecs list-tasks --cluster default
# Describe the Running Task
> aws ecs describe-tasks --cluster default --task task_ID
```


[оффициальное руководство](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
[AWS CLI ECS](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_AWSCLI.html)
