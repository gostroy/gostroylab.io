## CLI
install `sdkman`
```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
echo source "$HOME/.sdkman/bin/sdkman-init.sh" >> ~/.zshrc
```

`spring encrypt`
```
sdk install springboot 2.7.18
sdk use springboot 2.7.18
spring --version

spring install org.springframework.cloud:spring-cloud-cli:2.2.4.RELEASE

spring encrypt --key @spring-key abcdef
```