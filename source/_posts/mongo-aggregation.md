---
title: MongoDb Aggregation
date: 2017-04-28
---

**Исходные данные:** есть коллекция с обьектами, в одном из полей которого - массив. **Хотим:**  получить обьект с 
определенным содержимым поля этого вложенного массива, и при этом **хотим** развернуть подмассив на отдельные 
обьекты, и вывести лишь те которые соответствуют условию

![](mongo_aggregation_1.JPG)

![](mongo_aggregation_2.JPG)

 
**find()** никак здесь не поможет, но есть **aggregate()**
```
db.getCollection('appStoreResponse')
    .aggregate([
        {$match: {'receipt.in_app': {$elemMatch: {transaction_id: '1000000261826707'}}}},
        {$unwind: '$receipt.in_app'}, 
        {$match: {'receipt.in_app.transaction_id': '1000000261826707'}}
    ])
```



**задача**: вывести список `transaction_id` из массива под-елемента и количества их повторений, если повторений от 2 раз 


![](mongo_aggregation_3.JPG)


```
db.getCollection('appStoreResponse').aggregate([
        {$unwind: '$receipt.in_app'}, 
        {$project: {transaction_id: '$receipt.in_app.transaction_id', number: {$add: [1]}}}, 
        {$group : {_id : "$transaction_id", count: {$sum:"$number"}}}, 
        {$match: {count: {$gt:1}}},
        {$sort: {count: -1}}  
    ])
```


групировка по `deviceAdId`, выводим лишь те количество которых `>=2`, подсчитуем `общее количество`
```
db.getCollection('users').aggregate(
        {$project: {deviceAdId: '$deviceAdId', number: {$add: [1]}}}
        ,{$group : {_id : "$deviceAdId", count: {$sum:"$number"}}}
        ,{$match: {count: {$gt:1}}}
        ,{ $group: { _id: 'total', count: { $sum: 1 } } }
)
```
