---
title: Mongod on Debian
date: 2017-04-21
---

```
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
# echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
# sudo apt-get update
# sudo apt-get install -y mongodb-org

## rm -r /var/lib/mongodb/*
# sudo systemctl enable mongod.service
# sudo systemctl start mongod
# sudo systemctl status mongod

# sudo vim /etc/mongod.conf
 security:
  authorization: enabled
# sudo systemctl restart mongod

# mongod
> use admin;
> db.createUser({user:"intell", pwd:"PASSWORD", roles: [{role: "userAdminAnyDatabase", db:"admin"}]})

> use mobileSubscriptions
> db.createUser({user: "mobileSubscriptions", pwd: "PASSWORD", roles: ["readWrite"]}) 


```