---
title: Docker Errors
date: 2017-10-25
---

### No space left on device
```
DOCKER CLEANUP:
 docker volume rm $(docker volume ls -qf dangling=true)
docker rm $(docker ps -q -f 'status=exited')
docker system prune -a
```
