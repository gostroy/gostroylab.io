---
title: Docker. Utils
date: 2019-02-07
---

to run any version of utils without install them locally
```
docker run --rm -it wurstmeister/kafka:1.1.0 \
kafka-consumer-groups.sh --bootstrap-server HOST:9092 --group user-phone-book-PROD-processingGroup --describe
```
