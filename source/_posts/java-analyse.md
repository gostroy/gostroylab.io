---
title: Java. Analyse
date: 2020-04-10
---

## JMX
стандартные настройки для мониторинга по JMX
```
JAVA_OPTS="\
-Dcom.sun.management.jmxremote \
-Dcom.sun.management.jmxremote.port=5555 \
-Dcom.sun.management.jmxremote.local.only=false \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
"
```
есди ваш **jconsole/VisualVm** не подключаются, добавте параметр с внешним IP сервера
```
-Djava.rmi.server.hostname=`curl -s ifconfig.co`
```

**jconsole** - граф инструмент который показывает статистику jmx. но так же можно пользоватся любым профайлером

### JMC:  Java Mission Control
для реалтайм мониторинга и профилирования JVM. 
Он позволяет собрать очень много информации о JVM процессе, найти узкие места и проблемы, которые влияют на производительность.

можно увидеть динамику расхода памяти по методам и к тому же по потокам пока программа еще работает

Mission Control включает в себя `JMX консоль` и `Java Flight Recorder (JFR)`. 
С помощью JMX осуществяется взаимодействие с удаленным Java процессом, а JFR собирает данные о событиях.
```
-Dcom.sun.management.jmxremote=true
-Dcom.sun.management.jmxremote.port=8888
-Dcom.sun.management.jmxremote.authenticate=false
-Dcom.sun.management.jmxremote.ssl=false

-XX:+UnlockCommercialFeatures
-XX:+FlightRecorder
```
```
jmc
```

еше можно - запущу Java Flight Recorder
```
-XX:StartFlightRecording=disk=true,dumponexit=true,filename=/tmp/r2dbc.jfr,settings=profile,path-to-gc-roots=false,delay=1s,name=R2DBC
```

### MAT: Eclipse Memory Analyzer
подходит только для статического анализа дампа

## OOM
когда процес падает по OOm - надо что бы он
* автоматом создал heapDump (.hprof)
* убил себя, а не остался зависшим
```
-XX:+HeapDumpOnOutOfMemoryError \
-XX:+ExitOnOutOfMemoryError \
```

если вас беспокоит GC и вы грешите на него как на того из-за кого падает все по памяти, то
```
-XX:+HeapDumpBeforeFullGC
-XX:+HeapDumpAfterFullGC
```
Если используется параллельный коллектор, такой как CMS или G1, то FullGC можно считать отказоустойчивым

* дал какую либо нотификацию об этом - НЕ РАБОТАЕТ опция
```
-XX:OnOutOfMemoryError="mail -s 'FD: stage-1 OOM' gostroy@gmail.com < /dev/null"
```

 *  https://habr.com/ru/post/324144/ analyse tools
 * https://www.atlassian.com/blog/archives/so-you-want-your-jvms-heap make quick dump

## Optimisation
### MEM
```
-XX:+UseStringDeduplication
```
при включённом G1 GC. Он позволяет сэкономить на памяти, схлопнув дублирующиеся строки за счёт дополнительной нагрузки на процессор. 
Трейдоф между памятью и CPU зависит только от конкретного приложения и настройки самого механизма дедупликации.

### Disk
* логи приложения мы пишем в локальный сислог по UDP (не на жесткий диск).  Логи JVM будем писать в tmpfs.
* во время синхронизации тредов на сейфпойнте JVM может записывать через memory map файл /tmp/hsperfdata*, в который она экспортирует некоторую статистику. Отключаем её
```
-XX:+PerfDisableSharedMem
```

## Profiling
 * **native method** - не обращать на них внимания, так как в основном они спят(state=sleep), но профайлер не знает этого
### VisualVm
безплатный. 
 * имеет плагины. 
есть режим
 * **Sampler** который часто дает более толковую информацию чем Profiler (работает на основе дампа тхредов). Хорош лишь для больших методов (долго работающих по CPU)
 * **Profiling** нужно запускать приложение с JAVA_OPT
```
-Xverify:none
``` 

### JProfiler
платный. нужно присоединить агента к вашему java процессу
```
cd /opt
wget https://download-gcdn.ej-technologies.com/jprofiler/jprofiler_linux_11_1_2.tar.gz
tar xvf jprofiler_linux_11_1_2.tar.gz
rm jprofiler_linux_11_1_2.tar.gz
ln -sv jprofiler11.1.2 jprofiler
``` 
```
JAVA_OPTS="-agentpath:/opt/jprofiler/bin/linux-x64/libjprofilerti.so=port=8849,nowait"
```

### YourKit
платный. аналог JProfiler

### async-profiler
отлавливает те кейсы, которые не отлавливают профайлеры выше. НО - консольный интерфейс
```
mkdir /opt/async-profiler
cd /opt/async-profiler
wget https://github.com/jvm-profiling-tools/async-profiler/releases/download/v1.7/async-profiler-1.7-linux-x64.tar.gz
tar xvf async-profiler-1.7-linux-x64.tar.gz
rm async-profiler-1.7-linux-x64.tar.gz
```
cpu profiling
```
jps
/opt/async-profiler/profiler.sh -d 10 12165
```

 * https://www.youtube.com/watch?v=QiGrTvsCZmA&feature=emb_logo
 