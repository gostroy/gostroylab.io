---
title: Keytool
date: 2017-12-28
---

```
$ vim ~/tomcat8/conf/server.xml

       <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
    maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
    clientAuth="false" sslProtocol="TLS" alias="tomcat"
    keystoreFile="/home/appsqedit/.tomcatkeystore" keystorePass="changeit" />

```

```
просмотр сертификатов в хранилище
$ keytool -list -v -keystore /home/appsqedit/.tomcatkeystore

добавление CA сертификата в хранилище
$ keytool -list -v -keystore /home/appsqedit/.tomcatkeystore | grep caIssuers -A 1
$ wget http://crt.comodoca.com/COMODORSADomainValidationSecureServerCA.crt
$ cp -v /home/appsqedit/.tomcatkeystore{,_BAK}
$ keytool -import -trustcacerts -file COMODORSADomainValidationSecureServerCA.crt -alias comodoValidationCA -keystore /home/appsqedit/.tomcatkeystore
```

Import a signed primary certificate to an existing Java keystore
```
keytool -import -trustcacerts -alias skedit.io -file /home/appsqedit/skedit_io.crt -keystore /home/appsqedit/.tomcatkeystore
```

SSL+tomcat from @Alex
```
# vim /home/appsqedit/certs/domain.crt
-----BEGIN RSA PUBLIC KEY-----
...
-----END RSA PUBLIC KEY-----
# vim /home/appsqedit/key/domain.key
-----BEGIN RSA PRIVATE KEY-----
... another one
-----END RSA PRIVATE KEY-----

# openssl pkcs12 -export -in domain.crt -inkey domain.key -out server.p12 -name tomcat -CAfile ca.crt -caname root
# keytool -importkeystore -destkeystore /home/appsqedit/.skeditio_keystore -srckeystore server.p12 -srcstoretype PKCS12 -alias tomcat
```

import public certificate to keystore
```
openssl s_client -showcerts -connect api.fin.do:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > /tmp/mycertfile.pem
keytool -import -trustcacerts -alias api.fin.do -file /tmp/mycertfile.pem -keystore keystore.p12
```

import public+private certificate to keystore
```
# combines your SSL certificate api_fin_do.crt and your private key api.fin.do.key into a single file, api_fin_do.p12
openssl pkcs12 -export -in api_fin_do.crt -inkey api.fin.do.key -out api_fin_do.p12 -name api_fin_do -CAfile ca_1.crt -CAfile ca_2.crt
# import pkcs.p12 to existing keystore keystore.jks
keytool -importkeystore -destkeystore api_fin_do.jks -srckeystore api_fin_do.p12 -srcstoretype PKCS12 -alias api_fin_do
```