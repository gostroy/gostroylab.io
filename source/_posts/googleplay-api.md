---
title: Google Play + Google APIs + Oauth2
date: 2017-04-23
---

для использования **Google Play Developer API** с server-to-server авторизацией необходимо:

1. для включения возможности использовать **Google Play Developer API** с приложением
```
Google Play Developer Console > Settings > API access
Make a link to your "Linked Project".
```

2. создать **Service account keys** (лучше json-формата, с ним легче потом работать с api `google-api-client`)
```
API Manager -> Credentials > Service account keys 
```

3. теперь этого пользователя нужно добавить в **Google Play Developer Console**
```
Google Play Developer Console > Settings > API access > Service Account
```


[Google APIs, Credentials](https://console.developers.google.com/apis/credentials)

[Google Play Developer Console](https://play.google.com/apps/publish/)

[Пример кода для настройки `Service accounts`](https://developers.google.com/api-client-library/java/google-api-java-client/oauth2#service_accounts)
