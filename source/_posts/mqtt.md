## mqttx 
консольный клиент для MQTT https://mqttx.app/docs/cli/get-started

для вывода времени сообшения - лучше использовать формат **log**
```
mqttx init
? Select MQTTX CLI output mode Log
```

запуск консюмер
```
mqttx sub -t 'kft/emv' -h 'localhost' -p 1883 -v
```