---
title: Proxy in Java
date: 2018-01-17
---

### Console

gradle + http proxy
```
vim gradle.properties

#http proxy setup
systemProp.http.proxyHost=<IP>
systemProp.http.proxyPort=<PORT>
#systemProp.http.proxyUser=userid
#systemProp.http.proxyPassword=password
systemProp.http.nonProxyHosts=*.nonproxyrepos.com|localhost
#https proxy setup
systemProp.https.proxyHost=<IP>
systemProp.https.proxyPort=<PORT>
#systemProp.https.proxyUser=userid
#systemProp.https.proxyPassword=password
systemProp.https.nonProxyHosts=*.nonproxyrepos.com|localhost
```

gradle + SOCKS
```
vim gradle.properties

org.gradle.jvmargs=-DsocksProxyHost=<IP> -DsocksProxyPort=<PORT>
```

java + SOCKS
```
$ JAVA_OPTS="-DsocksProxyHost=127.0.0.1 -DsocksProxyPort=5433" ./bin/catalina.sh run
```

### MacOS
http proxy server [http://squidman.net/squidman/](http://squidman.net/squidman/)

socks client over ssh [https://www.opoet.com/pyro/](https://www.opoet.com/pyro/)