---
title: jhome
date: 2017-10-20
---


```
$ type jhome
jhome is a function
jhome ()
{
    export JAVA_HOME=`/usr/libexec/java_home $@`;
    echo "JAVA_HOME:" $JAVA_HOME;
    echo "java -version:";
    java -version
}
```
