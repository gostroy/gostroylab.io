---
title: Tcpdump
date: 2020-03-26
---

## HTTP
```
sudo tcpdump -i eth0 -A -s 0 '(((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)' | awk '{print strftime("%Y-%m-%d,%H:%M:%S,"$0)}' | grep HTTP
```