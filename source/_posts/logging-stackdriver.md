---
title: Logging. StackDriver
date: 2020-01-28
---

Решение от Google Cloud Platform https://console.cloud.google.com/logs/viewer

Для фильтрации можно использовать след фильтры, разделленые новой строкой каждый
```
httpRequest.requestUrl="https://domain/path"
httpRequest.status!=200
httpRequest.status=502

resource.type="http_load_balancer"
resource.labels.forwarding_rule_name="k8s-fws-default-basic-ingress--7e6d71ea68a52cd9"
```