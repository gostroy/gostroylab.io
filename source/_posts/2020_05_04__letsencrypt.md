---
title: LetsEncrypt
date: 2020-05-04
---

generate new certificate
```
sudo certbot certonly --server https://acme-v02.api.letsencrypt.org/directory --manual --preferred-challenges dns -d 'video.fin.do'
sudo ls /etc/letsencrypt/live/video.fin.do/
```

update exist certificates
```
sudo certbot renew
```
if will not work (let's update over nginx)
```
apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface
apt install python3-certbot-nginx  #plugin nginx для certbot
certbot --nginx -d api-staging2.fin.do
```
или вручную
```
certbot certonly --standalone --preferred-challenges http -d api-staging2.fin.do
```

import to keystore
```
openssl pkcs12 -export \
    -in /etc/letsencrypt/live/api-staging2.fin.do/fullchain.pem -inkey /etc/letsencrypt/live/api-staging2.fin.do/privkey.pem \
    -name api-staging2.fin.do -out /etc/letsencrypt/live/api-staging2.fin.do/api-staging2.fin.do.p12 \
    -CAfile /etc/letsencrypt/live/api-staging2.fin.do/chain.pem -caname root

cp /etc/letsencrypt/live/api-staging2.fin.do/api-staging2.fin.do.p12 /tmp/
chown sergey /tmp/api-staging2.fin.do.p12

scp findo-stage2:/tmp/api-staging2.fin.do.p12 /Users/panser/IdeaProjects/remote/findo/findo-backend/findo-shared/src/main/resources/ssl
```
and(maybe) if you need jks
```
keytool -importkeystore -deststorepass changeit \
    -destkeystore /etc/letsencrypt/live/api-staging2.fin.do/api-staging2.fin.do.jks \
    -srckeystore /etc/letsencrypt/live/api-staging2.fin.do/api-staging2.fin.do.p12 -srcstoretype PKCS12
```