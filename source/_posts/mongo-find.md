---
title: MongoDb Find
date: 2017-07-11
---

вывести записи с существующим определенным полем `$exists`
```
db.getCollection('subscription').find({appError: {$exists: true}})
```

```
db.getCollection('users').find({'forRetargeting': true}).count()
2003
```

```
db.getCollection('errors').find({}).sort({timestamp: -1}).limit(50)
```

обратиться к сложному подобьекту из `DbRef`
```
db.getCollection('appStoreResponse').find({'request.$id': '591dafd02c1ae77fd4a57cf9'})
```

найти все записи с `несуществующим полем` **ИЛИ** `с пустым массивом`
```
db.getCollection('appStoreResponse').find(
    {$or: [
        {'receipt.in_app':{$size: 0}}, 
        {'receipt.in_app':{$exists: false}}
    ]}
)
```

найти 50 последних записей **отсортиорованных** по дате создания
```
db.getCollection('appStoreResponse')
    .find({})
    .sort({createdDate:-1})
    .limit(50)
```

**нет**
```
db.getCollection('subscription')
`   .find({store: {$not: {$eq: 'AppleAppStore'}}})
```

поиск по елементам под-массива
```
db.getCollection('appStoreResponse')
    .find({'receipt.in_app': 
        {$elemMatch: {transaction_id: '1000000261827996'}}
        })
```