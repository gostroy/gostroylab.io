---
title: AWS Elastic Beanstalk
date: 2017-03-23
---

для разворачивания environment на AWS инфраструктуре (подобие Docker, но для AWS)
<!--more-->

```
> python --version

> curl -O https://bootstrap.pypa.io/get-pip.py
> python3 get-pip.py
> pip --version

> pip install --upgrade --user awsebcli
> eb --version
# для linux
$ echo 'export PATH=~/.local/bin/:$PATH' >> ~/.bashrc  #для linux консоли
$ sudo ln -sv /var/jenkins_home/.local/bin/eb /usr/local/bin/ # для Jenkins

# предварительно в AWS Console создать
# - IAM пользователя со всеми ролями что понадобяться вашему EB окружению
# - ssh keypair
> eb init
> eb create repengine-dev

> eb create -r ap-southeast-2 repengine-prod  # создать кластер в конкретном регионе
> eb create -s repengine-dev # использовать один инстанс EC2 без никаких load balancer

> eb list  # просмотр всех elastic beanstalk в дефолтном для пользователя регионе
> eb list -r ap-southeast-2  # просмотр всех elastic beanstalk в регионе ap-southeast-2

> eb ssh repengine-dev
> eb ssh repengine-test
> eb ssh repengine-prod -r ap-southeast-2

> eb deploy #задеплоить на AWS - HEAD git version
> eb deploy --staged  # задеплоить на AWS - последнии ф-лы из рабочей директории 
```

for **Docker env**
```
> eb init -p Docker
> vim Dockerrun.aws.json
...
> eb create -s jenkins
> eb deploy jenkins
> eb ssh jenkins
```


[Spring Boot AWS Elastic Beanstalk example](https://github.com/ExampleDriven/spring-boot-aws-elasticbeanstalk-example)

[оффициальные примеры приложений](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/java-getstarted.html)

[пример как задавать опции EB в ф-ле конфигурации](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/java-tomcat-platform.html#java-tomcat-platform-configfiles)

[все доступные опции](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-environmentprocess-process)