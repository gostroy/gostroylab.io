---
title: Kafka. Notes
date: 2017-10-09
---


## commands
```
# creaate topic
kafka-topics --create --zookeeper 10.16.0.16:2181 \
--replication-factor 1 --partitions 1 --topic findo-test

# список топиков
kafka-topics --list --zookeeper HOST:2181
# удалить топик (if delete.topic.enable is set to true)
kafka-topics --delete --zookeeper HOST:2181 --topic email-to-send-LOCAL

# записать сообщение в топик
kafka-console-producer --broker-list HOST:9092 --topic topic-name

# сообщения в топике
kafka-console-consumer --bootstrap-server HOST:9092 --topic springCloudBus --from-beginning

# список consumer(процессинговой) груп
kafka-consumer-groups --bootstrap-server HOST:9092  --list

# состояние обработки топика конкретной consumer(процессинговой) групой
kafka-consumer-groups --bootstrap-server HOST:9092 --group springCloudBus-LOCAL --describe
```

## Graphic Clients

графический клиент для *Kafka* [Kafka Tool](http://www.kafkatool.com/)

## Kafkacat
```
# brew install kafkacat --with-yajl
# brew install jq
```

администрирование. просмотр существующих брокеров/топиков/партиция_в_топике
```
$ kafkacat -L -b 127.0.0.1:9092
```

для тестирования *Consumer* (приема сообщений)
```
$ kafkacat -C -b 127.0.0.1:9092 -t kvstore_byte | jq
```

для тестирования *Producer* (отправка сообщений на kafka)
```
kafkacat -P -b 127.0.0.1:9092 -t kvstore_byte
^C  # для отправки выше написаного сообщения
```

тестирование всего вместе
```
# записать в кафку
kafkacat -b IP:9092 -P -t test 
> lalalalalala
# прочитать из кафки
kafkacat -b IP:9092 -C -t test
> lalalalalala
```


Transferring messages between topics
```
$ kafkacat -C -b kafka -t awesome-topic -e | kafkacat -P -b kafka -t awesome-topic2
## OR
$ kafkacat -C -b kafka -t awesome-topic -e > awesome-messages.txt
$ cat awesome-messages.txt | kafkacat -P -b kafka -t awesome-topic2
```
Transferring messages between clusters
```
kafkacat -C -b kafka2 -t awesome-topic -e | kafkacat -P -b kafka -t awesome-topic
```

Get the messages you want
```
seq 1 100 | kafkacat -P -b kafka -t superduper-topic
```
read 5 messages from the start (from each partition that you have: *I have 10 partitions so I get 50 messages.*)
```
kafkacat kafka -t superduper-topic -o 5 -e
```
read 5 messages from the end
```
kafkacat kafka -t superduper-topic -o -5 -e
```
If we wanted to focus on one partition
```
kafkacat -C -b kafka -t superduper-topic -o -5 -e -p 5
```

## landoop/kafka
```
version: '3'
services:
  kafka:
    container_name: app-kafka
    image: landoop/fast-data-dev
    ports:
      - 2181:2181           # Zookeeper
      - 3030:3030           # Landoop UI
      - 8081-8083:8081-8083 # REST Proxy, Schema Registry, Kafka Connect
      - 9581-9585:9581-9585 # JMX Ports
      - 9092:9092           # Kafka Broker
    environment:
      ADV_HOST: app.domain.com
      RUNTESTS: 0
      FORWARDLOGS: 0
      SAMPLEDATA: 0
    networks:
      - fast-data

networks:
  fast-data:

# Start-up
# docker-compose up

# Clean-up
# docker-compose down --volume


# docker run --rm -p 2181:2181 -p 3030:3030 -p 8081-8083:8081-8083 \
#        -p 9581-9585:9581-9585 -p 9092:9092 -e ADV_HOST=app.domain.com \
#        landoop/fast-data-dev

# docker run --rm -it --net=host landoop/fast-data-dev bash

```

## Errors && Analyz
**reset offset**
```
# create new consumer-group for reset
kafka-console-consumer --bootstrap-server HOST:9092 --topic user-phone-book-PROD --consumer-property group.id=user-phone-book-PROD-processingGroup77

# reset
kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --reset-offsets --to-datetime 2019-01-17T00:00:00.000
kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --reset-offsets --to-datetime 2019-01-17T00:00:00.000 --execute

kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --reset-offsets --shift-by -16

kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --reset-offsets --shift-by 210
kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --reset-offsets --shift-by 210 --execute
...

# for old clients add - --topic
kafka-consumer-groups --bootstrap-server HOST:9092 --topic user-phone-book-PROD --group user-phone-book-PROD-processingGroup --topic user-phone-book-PROD --reset-offsets --shift-by 210 --execute
```

**LAG (LOG-END-OFFSET)**
```
посмотреть состояние очереди на обрабоку топика консьюмером на определенныю consumer-group
kafka-consumer-groups --bootstrap-server kafka:9092 --group user-phone-book-PROD-processingGroup --describe
```

**Broker: Leader not available**
```
$ kafkacat -L -b 127.0.0.1:9092
Metadata for all topics (from broker -1: 127.0.0.1:9092/bootstrap):
 1 brokers:
  broker 1006 at localhost:9092
 4 topics:
  topic "kvstore_byte" with 1 partitions:
    partition 0, leader -1, replicas: 1001, isrs: , Broker: Leader not available
  topic "__consumer_offsets" with 50 partitions:
    partition 23, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 41, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 32, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 8, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 17, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 44, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 35, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 26, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 11, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 29, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 38, leader -1, replicas: 1001, isrs: , Broker: Leader not available
    partition 47, leader -1, replicas: 1001, isrs: , Broker: Leader not available
```

возможная причина
```
???
broker 1006 
replicas: 1001
```
как по мне, эти номера должны быть одинаковы. мне помогло
```
docker-compose down
docker-compose up
```

**LEADER_NOT_AVAILABLE**
```
2019-02-14 23:49:29.017  WARN [bootstrap,,,] 57142 --- [ad | producer-1] org.apache.kafka.clients.NetworkClient   : Error while fetching metadata with correlation id 441 : {user-phone-book-LOCAL3=LEADER_NOT_AVAILABLE}
2019-02-14 23:49:29.157  WARN [bootstrap,,,] 57142 --- [ad | producer-1] org.apache.kafka.clients.NetworkClient   : Error while fetching metadata with correlation id 442 : {user-phone-book-LOCAL3=LEADER_NOT_AVAILABLE}
```
при проверки я выяснил,что `топик` был создан автоматом, но вот `группа` - нет
```
$ kafka-topics --list --zookeeper kafka:2181
user-phone-book-LOCAL3
...

$ kafka-consumer-groups --bootstrap-server kafka:9092  --list
...
```

## auth
### server configuration
```
cat /opt/kafka/config/kafka_jaas.conf


KafkaServer {

org.apache.kafka.common.security.plain.PlainLoginModule required

username="superman"

password="dkmdmkdmk9393jefdmdedjd8338ejfjcm"
user_superman="dkmdmkdmk9393jefdmdedjd8338ejfjcm"

user_user1="dnj383uejcmcmddd"
user_user2="54444rfffvr4ee"
user_user3="vvgf4efr43edvgtr"
;

};
```

### client configuration
SASL_PLAINTEXT
```
cat ~/kafka/client.properties
sasl.mechanism=PLAIN
security.protocol=SASL_PLAINTEXT

cat ~/kafka/jaas-dev.conf
KafkaClient {

org.apache.kafka.common.security.plain.PlainLoginModule required
username="USERNAME"
password="PASSWORD";

};
```

SASL_SSL
```
cat client.properties
sasl.mechanism=SCRAM-SHA-512
security.protocol=SASL_SSL
ssl.truststore.location=/opt/homebrew/Cellar/openjdk/19.0.2/libexec/openjdk.jdk/Contents/Home/lib/security/cacerts

cat jaas-dev.conf
KafkaClient {

org.apache.kafka.common.security.scram.ScramLoginModule required
username="USERNAME"
password="PASSWORD";

};
```

test
```
cd ~/kafka/
kafka-topics --zookeeper kafka-dev.lyricist.one:2181  --list

KAFKA_OPTS="-Djava.security.auth.login.config=jaas-stage2.conf" \
kafka-console-consumer --bootstrap-server IP:9093 \
--consumer.config client.properties \
--topic springCloudBusOutput --from-beginning

KAFKA_OPTS="-Djava.security.auth.login.config=jaas-stage2.conf" \
kafka-consumer-groups --bootstrap-server IP:9093 \
--command-config client.properties \
--list 

KAFKA_OPTS="-Djava.security.auth.login.config=jaas-stage2.conf" \
kafka-consumer-groups --bootstrap-server IP:9093 \
--command-config client.properties \
--group springCloudBus-STAGE2 --describe

```
