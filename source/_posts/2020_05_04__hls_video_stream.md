---
title: HLS: mp4 streaming
date: 2020-05-04
---

convert mp4 to hls files
```
ffmpeg -i screen-only_9-16_v002.mp4 -profile:v baseline -level 3.0 -start_number 0 -hls_time 10 -hls_list_size 0 -f hls index.m3u8
```

upload mp4 && all new files to `S3`

configure `CloudFront` to get access to previous s3 bucket

create wildcard ssl certificate on `AWS ACM`

with exist ssl certificate you can configure `CNAME` for cloudFront domain to your domain
```
* in CloudFront configuration - cname
* in your dns domain - cname
```

and finally your video stream is ready
```
http://video.fin.do/invite/index.m3u8
```