---
title: MySQL access
date: 2018-01-08
---

ro
```
CREATE USER 'admin_ro'@'%' IDENTIFIED BY 'XXXXXXX';
GRANT SELECT ON *.* TO 'admin_ro'@'%';
GRANT EXECUTE ON *.* TO 'admin_ro'@'%';
FLUSH PRIVILEGES;
```

