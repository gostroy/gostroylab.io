---
title: Docker on DigitalOcean
date: 2017-04-08
---

```
# создать docker-droplet
docker-machine create --driver digitalocean --digitalocean-access-token xxxxx docker-repEngine
docker-machine ls
docker-machine env docker-repEngine
docker-machine inspect docker-repEngine
 
# скопировать ф-л на машину
docker-machine scp repEngine.war docker-repEngine:/
# подключиться к машине
docker-machine ssh docker-repEngine
 
# удалить
docker-machine stop docker-repEngine
docker-machine rm docker-repEngine
docker-machine ls
```

[install docker-machine in linux](https://www.digitalocean.com/community/tutorials/how-to-provision-and-manage-remote-docker-hosts-with-docker-machine-on-ubuntu-16-04)

[create VM with docker on DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-provision-and-manage-remote-docker-hosts-with-docker-machine-on-ubuntu-16-04)
