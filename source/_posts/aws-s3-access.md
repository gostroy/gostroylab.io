---
title: Amazon S3. check access
date: 2017-10-09
---

для проверки доступа к S3 обьекту

```
$ cat ~/.aws/config | grep vidpost -A4
[profile vidpost]
region = us-east-1
aws_access_key_id = XXX
aws_secret_access_key = XXX

$ aws s3api get-object --bucket vidpostsystem-config --key vidpost/vidpost.properties vidpost.properties --profile repengine-test-s3

An error occurred (AccessDenied) when calling the GetObject operation: Access Denied
```

```
$ cat ~/.aws/config | grep repen -A4
[profile repengine-test-s3]
aws_access_key_id = XXX
aws_secret_access_key = XXX
$ aws s3api get-object --bucket repengine-avator-764737e2-03df-4aca-9bc3-059bc62e5f31-awstest --key 008ac382-b946-4a26-80c2-d9119bc24a9e-200-200.jpeg 008ac382-b946-4a26-80c2-d9119bc24a9e-200-200.jpeg --profile repengine-test-s3
{
    "AcceptRanges": "bytes",
    "ContentType": "image/jpeg",
    "LastModified": "Wed, 23 Nov 2016 10:57:53 GMT",
    "ContentLength": 8024,
    "ETag": "\"21296fd39f108637bf40ad4298b3b52e\"",
    "Metadata": {}
}
```