---
title: Gradle Exec
date: 2017-04-26
---


### несколько комманд подряд
запуск **последовательности комманд в linux**
```
task deploy_elasticbeanstalk() {
    description 'deploy application to AWS ElasticBeansTalk environment, should run with options -P awsdev|awstest|prod'
    doLast {
        deploy_elasticbeanstalk_steps()
    }
}


void deploy_elasticbeanstalk_steps() {
    exec {
        commandLine 'eb', 'use', EB_ENVIRONMENT, '-r', EB_ZONE
    }
    exec {
        commandLine 'eb', 'setenv', 'SPRING_PROFILES_ACTIVE=' + PROFILES, '-r', EB_ZONE
    }
    exec {
        commandLine 'eb', 'deploy', '--staged', '-v', '-r', EB_ZONE
    }
}


```

запуск **последовательности комманд в windows**
```
    commandLine 'cmd', '/c', '
        (@FOR /f "tokens=*" %i IN ('docker-machine env default') DO @%i) & 
        docker run --rm -d -p 8983:8983 --name my_solr solr & 
        sleep 15 & 
        docker logs my_solr 
```

### запуск внешнего скрипта
запуск **скрипта в linux**
```
task startDocker() {
     doLast {
         exec {
              executable "./login.sh"
          }
     }
}
```

### зависимости мужду task-ами
```
tasks["test"].dependsOn("runExecProcess")
tasks["test"].finalizedBy tasks["stopExecProcess"]
```

### выполение комманды любого синтаксиса в linux  

так же выполнить консольную комманду можно всмомощу самописного метода
```
static runCmd(String cmd, String errorMessage) {
    def realcmd = ["/bin/bash", "-c", cmd]
    println(cmd)
    def proc = realcmd.execute()
    def sout = new StringBuffer()
    
    proc.consumeProcessOutput(sout, sout)
    proc.waitForProcessOutput()
    println(sout)
    if (proc.exitValue() != 0) {
        throw new GradleException(errorMessage)
    }
}
```

```
runCmd("\$(${cmd})", "failed to log in")
```

и сама комманда для которой все это городилось
```
# $(aws ecr get-login --region us-east-1)
```