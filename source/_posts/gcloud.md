---
title: gcloud. Google Cloud Engine
date: 2017-10-31
---

## Login to `Compute Engine` instance
```
$ cd ~/.ssh
$ ln -sv id_rsa google_compute_engine
$ ln -sv id_rsa.pub google_compute_engine.pub
$ ln -sv known_hosts google_compute_known_hosts

$ gcloud auth login
$ gcloud config set project ride6th
$ gcloud compute ssh app-riders

# and after
$ ssh 35.203.128.137 -l panser
```
