---
title: AWS. Instance Metadata and User Data
date: 2017-10-15
---

для получение различной информации о запущенном инстансе EC2
```
$ curl http://169.254.169.254/latest/meta-data/
ami-id
ami-launch-index
ami-manifest-path
block-device-mapping/
hostname
instance-action
instance-id
instance-type
local-hostname
local-ipv4
mac
metrics/
network/
placement/
profile
public-hostname
public-ipv4
public-keys/
reservation-id
security-groups

$ curl http://169.254.169.254/latest/meta-data/public-ipv4
52.29.81.62

$ curl http://169.254.169.254/latest/meta-data/hostname
ip-172-31-17-178.eu-central-1.compute.internal

$ curl http://169.254.169.254/latest/meta-data/public-hostname
ec2-52-29-81-62.eu-central-1.compute.amazonaws.com

$ curl http://169.254.169.254/latest/meta-data/public-keys/
0=rep_engine_demo_aws

...
```

