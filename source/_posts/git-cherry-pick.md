---
title: GIT. Cherry-Pick
date: 2020-04-06
---

```
your local changes would be overwritten by cherry-pick. hint: commit your changes or stash them to proceed. cherry-pick failed
```

to fix
```
git reset --hard
```

