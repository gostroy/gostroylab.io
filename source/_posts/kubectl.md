
## TIPS
переключатся между кластара на `aws eks`
```
aws eks update-kubeconfig --region eu-central-1 --name cluster-name
```
ИЛИ конфигурация
```
~/.kube/config

kubectl config get-contexts
kubectl config current-context
kubectl config use-context
kubectl config set-context
```
ИЛИ
```
kubectx
kubens
```

поды
```
список под
kubectl get pods

логи
kubectl logs pod-prod-6fdc5d8754-s9zgt

sh
kubectl exec -it  pod-prod-6fdc5d8754-s9zgt -- sh
```

поды и логи
```
лог pod'а c человекочитаемым timestamp на случай его отсутствия:
kubectl -n my-namespace logs -f my-pod --timestamps

логи со всех контейнеров pod'а:
kubectl -n my-namespace logs -f my-pod --all-containers

логи предыдущего контейнера, который, к примеру, упал:
kubectl -n my-namespace logs my-pod --previous
```

поды и сортиовка & выборка
```
поды в незапущенос состоянии
kubectl get pods -A --field-selector=status.phase!=Running | grep -v Complete

Сортировка списка pod'ов по количеству рестартов
kubectl get pods --sort-by=.status.containerStatuses[0].restartCount

Получить по каждому контейнеру каждого pod'а его limits и requests:
kubectl get pods \
 -o=custom-columns='NAME:spec.containers[*].name,MEMREQ:spec.containers[*].resources.requests.memory,MEMLIM:spec.containers[*].resources.limits.memory,CPUREQ:spec.containers[*].resources.requests.cpu,CPULIM:spec.containers[*].resources.limits.cpu'
```

ноды (сервера)
```
список узлов с указанием объема их оперативной памяти
kubectl get no -o json | \
  jq -r '.items | sort_by(.status.capacity.memory)[]|[.metadata.name,.status.capacity.memory]| @tsv'

список узлов и количество pod'ов на них  
kubectl get po -o json --all-namespaces | \
  jq '.items | group_by(.spec.nodeName) | map({"nodeName": .[0].spec.nodeName, "count": length}) | sort_by(.count)'

внутренние IP-адреса узлов кластера
kubectl get nodes -o json | \
  jq -r '.items[].status.addresses[]? | select (.type == "InternalIP") | .address' | \
  paste -sd "\n" -
```

ресурсы
```
# cpu
kubectl top pods -A | sort --reverse --key 3 --numeric
# memory
kubectl top pods -A | sort --reverse --key 4 --numeric
```

deployment
```
# перезапустть существуюший деплоймент
kubectl rollout restart deployment/x24-pci-dss-pos-tieta-prod
```

минифест
```
получить манифест требуемой сущности
kubectl run test --image=grafana/grafana --dry-run -o yaml

и после применить его и создать ресурсы
kubectl apply -f ./my-manifest.yaml 
```

man
```
kubectl explain hpa
```


## LINKS
  * https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/
  * 