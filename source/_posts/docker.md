---
title: Docker
date: 2017-04-08
---

```
# удалить все контейнеры
docker ps -a | awk '{print $1}' | xargs --no-run-if-empty docker rm
# удалить все образы
docker images | awk '{print $3}' | xargs docker rmi
 
# собрать образ из Dockerfile
mkdir -p docker/repEngine-mysql 
cd docker/repEngine-mysql 
vim Dockerfile
...
docker build -t panser/rep_engine-mysql .
docker images
 
# создать контейнер из образа, и запустить его
docker run -it --rm --name repengine-mysql -h repengine-mysql panser/repengine-mysql
# создать контейнер из образа, и запустить его: + 
#   * експортировать внутренний порт 8080 на 8080
#   * связать его с другим контейнером (repengine-mysql, будет доступен по dns)
#   * примонтировать внешнюю директорию /vagrant/docker/repEngine-java во внутрь контейнера, в /docker
docker run -it --rm -p 8080:8080 -v /vagrant/docker/repEngine-java:/docker --name repengine-java -h repengine-java --link repengine-mysql panser/repengine-java
# проверить связь с другим контейнером
docker inspect -f &amp;amp;amp;quot;{{ .HostConfig.Links }}&amp;amp;amp;quot; repengine-java
root@repengine-java:/# ping repengine-mysql

# все сразу
$ docker build -t panser/repengine-jenkins . && docker run -it --rm --name repengine-jenkins panser/repengine-jenkins
 
# просмотреть запущенные контейнеры
docker ps
# просмотреть все контейнеры
docker ps -a
 
# запустить созданный ранее контейнер
docker ps -a
docker start repengine-mysql
 
# просмотреть логи в контейнере
docker logs repengine-mysql
# подключиться к запущенному контейнеру в активную сессию
docker attach repengine-mysql
# подключиться к запущенному контейнеру в новую сессию
docker exec -it repengine-mysql bash
# запустить и подключиться к контейнеру
docker run --rm -it --entrypoint bash x24-camel-karavan:latest
# подключиться к запущенному контейнеру в новую сессию от пользователя ROOT
docker exec -it -u root repengine-mysql bash
 
# остановить контейнер
docker stop repengine-mysql
 
# залогиниться на DockerHub
docker login
# найти подходящий image-образ
docker search
# просмотреть локальные образы в системе
docker images
# залить образ в hub.docker.com
docker push panser/repengine-java
 
```
