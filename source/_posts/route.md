---
title: routing
date: 2021-07-09
---

## Mac OS
```
netstat -rn
```

```
brew install iproute2mac
ip route get h2.appservice.tech
ip r s
```

посмотреть меняет ли маршрутизацию подключение к вашей OpenVpn (или чему подобному)
```
% ip route get i.ua
91.198.36.14 via 192.168.88.1 dev en0  src 192.168.88.131

% ip route get i.ua
91.198.36.14 via 10.108.0.1 dev utun2  src 10.108.0.3
```
в данном случае - меняет. это не очень хорошо, так как сильнее нагружает VPN трафиком на каждый домен.

## telnet
альтернатива
```
ssh 10.32.0.5 -p 6379 -vvv
```