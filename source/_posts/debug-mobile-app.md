---
title: Debug mobile app
date: 2019-07-19
---

## HTTP Proxy
### mitm
#### http proxy
install https://mitmproxy.org and run web interface
```
brew install mitmproxy
```
```
mitmweb -p 443 --set web_port=8012
mitmweb -p 443 --set web_port=8012 --cert ~/.ssh/other/facedrive.com.pem
```

after configure your local device wifi connection to work over your local computer ip, port (-p)
```
ifconfig
```
перейдем в браузере по адресу http://mitm.it и нажмем на иконку Apple для скачивания сертификата Mitmproxy.

#### reverse proxy
```
mitmweb -p 443 --set web_port=8012 --mode=reverse:http://localhost:8040 \ 
--cert api-staging.fin.do=api_staging_fin_do.pem --cert api.fin.do=api_fin_do.pem
```
example of cert file
```
vim api_fin_do.pem
-----BEGIN PRIVATE KEY-----
...
-----END PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
...
-----END CERTIFICATE-----
...other certs
```


### burp
известный сетевой тул с большим количеством функционала. 
Правда reverse-proxy у меня не получилось на нем настроить 
для анализа запросов которые приходят на локальную LOCAL среду разработки

## dns server
### dnsmasq
```
brew install dnsmasq
mkdir -pv $(brew --prefix)/etc/
echo 'address=/facedrive.com/<YOUR_INTERNAL_IP>' > $(brew --prefix)/etc/dnsmasq.conf
echo 'address=/stage-pl-dp.facedrive.com/<YOUR_INTERNAL_IP>' > $(brew --prefix)/etc/dnsmasq.conf
echo 'address=/pre-prod-plat-lb.facedrive.com/<YOUR_INTERNAL_IP>' > $(brew --prefix)/etc/dnsmasq.conf

sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
sudo launchctl unload -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
```

after you should configure your device to use this fictive dns-server instead default one.
for this I install on Android https://play.google.com/store/apps/details?id=com.burakgon.dnschanger