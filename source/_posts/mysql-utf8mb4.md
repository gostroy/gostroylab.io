---
title: MySQL + smiles in db
date: 2017-12-04
---

we should use for this **utf8mb4** encoding: for server, for table columns. And YES, we should **configure mysqld for this**

```
$ vim $(brew --prefix)/etc/my.cnf

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci

[client]
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4
```
```
$ /usr/local/opt/mysql/support-files/mysql.server restart
```

and then check character/collation in server/columns
```
SHOW VARIABLES WHERE Variable_name LIKE 'character\_set\_%' OR Variable_name LIKE 'collation%';

```
```
SELECT TABLE_NAME,COLUMN_NAME,CHARACTER_SET_NAME,COLLATION_NAME FROM information_schema.`COLUMNS`
WHERE table_schema = "sqedit"
  AND table_name = "post";
```

and change if needed
```
ALTER TABLE attach CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE post CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
...

```