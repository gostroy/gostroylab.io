---
title: Google Cloud Speech API
date: 2017-05-16
---

```
>gcloud auth activate-service-account --key-file=key\TT-googleSpeachApiTest-61f5f787716f.json
>gcloud auth list   # смотрим на ACTIVE

>gcloud auth application-default login
>gcloud auth application-default print-access-token
access_token

> vim sync-request.json
{
  "config": {
      "encoding":"FLAC",
      "sampleRateHertz": 16000,
      "languageCode": "en-US"
  },
  "audio": {
      "uri":"gs://cloud-samples-tests/speech/brooklyn.flac"
  }
}
# Synchronous Speech Recognition, returns the recognized text for short audio (less than ~1 minute) 
> curl -s -k -H "Content-Type: application/json" \
	-H "Authorization: Bearer access_token" \
	https://speech.googleapis.com/v1/speech:recognize -d @sync-request.json
# Asynchronous Speech Recognition, for long running audio processing 
> curl -s -k -H "Content-Type: application/json" \
	-H "Authorization: Bearer access_token" \
	https://speech.googleapis.com/v1/speech:longrunningrecognize -d @sync-request.json	
{
  "name": "4184610840413844032"
}	
> curl -X GET -H "Authorization: Bearer YOUR_API_KEY" https://speech.googleapis.com/v1/operations/YOUR_OPERATION_NAME
{
  "name": "4184610840413844032",
  "metadata": {
    "@type": "type.googleapis.com/google.cloud.speech.v1.LongRunningRecognizeMetadata",
    "progressPercent": 100,
    "startTime": "2017-05-14T13:15:51.822864Z",
    "lastUpdateTime": "2017-05-14T13:15:53.441575Z"
  },
  "done": true,
  "response": {
    "@type": "type.googleapis.com/google.cloud.speech.v1.LongRunningRecognizeResponse",
    "results": [
      {
        "alternatives": [
          {
            "transcript": "how old is the Brooklyn Bridge",
            "confidence": 0.98267895
          }
        ]
      }
    ]
  }
} 
```
