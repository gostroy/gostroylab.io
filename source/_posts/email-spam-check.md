---
title: Email: check on spam
date: 2019-10-09
---

## Configure before send - ONCE
надо зарегистрировать домен с которого отправляете рассылки в важных для вашей контактной базы постмастерах
```
https://postmaster.google.com/

``` 

## email validation
 * https://clearout.io
 * https://emailcheckerpro.com
 * https://emailchecker.biz

## Check services - EACH TIME
 * https://www.subjectline.com проверить **title** письма на спам
 * https://talosintelligence.com/reputation_center/lookup check domain/IP
 * https://www.mail-tester.com/  - check IP/SPF/DKIM/SpamAssassin Score/content/blacklists. Good UI interface, userfriendly
     * аналоги, но похуже UI:
     * http://www.isnotspam.com  - проверить письмо что вы собираетесь отправить на разные антиспам-заголовки
     * https://www.unlocktheinbox.com   free version available; for more advanced users: it offers some technically detailed reports but no general score like Mail-Tester
 * https://spamcheck.postmarkapp.com  - берем письмо со всеми заголовками, и проверяем здесь на **SpamAssassin Score**. Important: **the lower score you get here, the better**. 
 Anything close to 5 or higher will be most probably automatically marked as spam by your prospects’ email providers. 
 * https://app.glockapps.com/ - рассылает писмо по списку тестовых-email, и показывает где имено оно попало в спам
 talosintelligence